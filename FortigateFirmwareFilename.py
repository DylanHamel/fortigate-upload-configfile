def getFortigateFirmwareFilename (model, version) :
    """
    This file is use for know file name for a Fortigate firmware.
   unfortunately, this is a static file.

    It starts since Fortigate 5.6.0

    If you want file name for Fortigate 60D version 5.6.3
    return = FGT_60D-v5-build1547-FORTINET.out

    :param model:       Fortigate models    (31E, 60D, 81E, etc.)
    :param version:     Fortigate firmware  (5.6.2, 6.0.0, 6.0.2, etc.)
    :return: firmwareFilename name of file for upgrade Fortigate
    """
    print("========================================================================")
    print("[getFortigateFirmwareFilename] Receive - |", model, "| and |", version, "|")

    # Available Models - this is written to make it easier to add
    fortigate = list()
    fortigate.append("40C") # For test
    fortigate.append("30E")
    fortigate.append("60D")
    fortigate.append("81E")
    fortigate.append("61E")
    fortigate.append("80D")
    fortigate.append("100D")
    fortigate.append("200D")
    # If you want add models just add a line here
    # Ex: fortigate.append("501E")

    # For check if model gived in parameter is in list/is valid
    # Nothing to do if you add model
    goodModel = False
    for modelInList in fortigate :
        if modelInList.find(model) != -1 :
            goodModel = True

    if not goodModel :
        print("[getFortigateFirmwareFilename] Return - |", "Not Good model", "| and |", False, "|")
        return "Not Good model", False

    # Available Firmware
    version5211 = "5.2.11" # For test with 40C
    version5213 = "5.2.13" # For test with 40C
    version542 = "5.4.2"
    version560 = "5.6.0"
    version561 = "5.6.1"
    version562 = "5.6.2"
    version563 = "5.6.3"
    version564 = "5.6.4"
    version565 = "5.6.5"
    version566 = "5.6.6"
    version567 = "5.6.7"
    version600 = "6.0.0"
    version601 = "6.0.1"
    version602 = "6.0.2"
    version603 = "6.0.3"
    version604 = "6.0.4"

    # If you want add version add a line here and add an "elif" condition below
    # Ex: version541 = "5.4.1"

    # ####
    # Exceptions
    # Fortigate 81E version 5.4.2 doesn't exist
    if (model in "81E" and version in version542) or \
        (model in "40C" and version not in (version5211 or version5213)) :
        print("[getFortigateFirmwareFilename] Return - |", "Model not compatible with version", "| and |", False, "|")
        return "Model not compatible with version", False
    
    # For check if version gived in parameter is valid
    if version604.find(version) != -1:
        buildVersion = "v6-build0231-FORTINET.out"
    elif version603.find(version) != -1 :
        buildVersion = "-v6-build0200-FORTINET.out"
    elif version602.find(version) != -1 :
        buildVersion = "-v6-build0163-FORTINET.out"
    elif version601.find(version) != -1 :
        buildVersion = "-v6-build0131-FORTINET.out"
    elif version600.find(version) != -1 :
        buildVersion = "-v6-build0076-FORTINET.out"
    elif version567.find(version) != -1 :
        buildVersion = "-v5-build1653-FORTINET.out"
    elif version566.find(version) != -1 :
        buildVersion = "-v5-build1630-FORTINET.out"
    elif version565.find(version) != -1 :
        buildVersion = "-v5-build1600-FORTINET.out"
    elif version564.find(version) != -1 :
        buildVersion = "-v5-build1575-FORTINET.out"
    elif version563.find(version) != -1 :
        buildVersion = "-v5-build1547-FORTINET.out"
    elif version562.find(version) != -1:
        buildVersion = "-v5-build1486-FORTINET.out"
    elif version561.find(version) != -1:
        buildVersion = "-v5-build1484-FORTINET.out"
    elif version560.find(version) != -1:
        buildVersion = "-v5-build1449-FORTINET.out"
    elif version5213.find(version) != -1:
        buildVersion = "-v5-build0762-FORTINET.out"
    elif version5211.find(version) != -1:
        buildVersion = "-v5-build0754-FORTINET.out"
    elif version542.find(version) != -1:
        buildVersion = "-v5-build1100-FORTINET.out"
    # add an "elif" here
    # Ex : elif version541.find(version) != -1:
    #           buildVersion = "v5...."
    else :
        print("[getFortigateFirmwareFilename] Return - |", "Not good version", "| and |", False, "|")
        return "Not good version", False

    print("[getFortigateFirmwareFilename] Return - |", ("FGT_" + model + buildVersion), "| and |", True, "|")
    return ("FGT_" + model + buildVersion), True

