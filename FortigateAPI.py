"""
This Class is an API for get forigate informations
"""

# For HTTP/S requests
import requests
import urllib3
# ------------------------------------------------------------------------------------------------------------
#
#
#
#
class FortigateAPI :
    """
    FortigateAPI object parameters
    * Fortigate username
    * Fortigate password
    * Fortigate connexion ipAddress
    * Fortigate connexion port
    * Cookie
    * Cookies connexion
    * Connexion method if https ou http
    * Fortigate Version     Example v6.0.0.
    * Fortigate Model       Example 81E
    """

    def __init__ (self, username, password, ipAddress, port=99999, sslCertificat=False, useHTTPS=False) :
        """
        :param username:        Fortigate username
        :param password:        Fortigate password
        :param ipAddress:       Fortigate ipAddress
        :param port:            Fortigate port
        :param sslCertificat:   Fortigate sslCertificat
        :param useHTTPS:        Fortigate useHTTPS true if HTTPS is used

        """

        print("[FortigateAPI] - ", username, password, ipAddress, port)

        # Check for avoid ssl =! TRUE or FALSE.
        assert useHTTPS == False or useHTTPS == True
        assert sslCertificat == False or sslCertificat == True


        self._username = username
        self._password = password
        self._ipAddress = ipAddress
        self._listCookies = list()

        usernamePassword = {'username': username, 'secretkey': password}

        # We define connexion method
        if useHTTPS :
            self._connectionMethodAddress = "https://" + ipAddress
            if port == 99999 :
                self._port = 443
            else :
                self._port = port

        else:
            self._connectionMethodAddress = "http://" + ipAddress
            if port == 99999 :
                self._port = 80
            else :
                self._port = port

        # Information about Secure Connection
        self._sslCertificat = sslCertificat

        # Print informations

        print("[FortigateAPI __init__] port - ", self._port)
        print("[FortigateAPI __init__] username - ", self._username)
        print("[FortigateAPI __init__] password - ", self._password)
        print("[FortigateAPI __init__] ipAddress - ", self._ipAddress)

        # Connexion on Fortigate
        try:
            print("[FortigateAPI __init__] requests - ", self._connectionMethodAddress + ":" + str(self._port) + '/logincheck')

            # For avoid InsecureRequestWarning error
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
            request = requests.post(self._connectionMethodAddress + ":" + str(self._port) + '/logincheck', usernamePassword, verify=self._sslCertificat)
            self._cookies = request.cookies
        except Exception as e :
            print(e)

        for c in self._cookies :
            self._listCookies.append(c)

        # This block will use Fortigate API for have Fortigate Model and Version
        path = "/api/v2/monitor/system/firmware"
        print("[FortigateAPI __init__] - ", self._connectionMethodAddress + ":" + str(self._port) + path)
        get = requests.get(self._connectionMethodAddress + ":" + str(self._port) + path ,
                           verify=self._sslCertificat, cookies=self._cookies)


        print("[FortigateAPI __init__] - ", get)

        getJSON = get.json()
        print("[FortigateAPI __init__] model   - ", getJSON["results"]["current"]["platform-id"][3:])
        print("[FortigateAPI __init__] version - ",getJSON["version"])
        self._version = getJSON["version"]
        self._model = getJSON["results"]["current"]["platform-id"][3:]


    # ALL GETTERS
    def getFortigateModel(self) :
        return self._model

    def getFortigateVersion(self) :
        return self._version

    def getFortigateUsername(self) :
        return self._username

    def getFortigatePassword(self) :
        return self._password

    def getFortigateConnexionCookie(self) :
        return self._cookies


    # ALL PRINTERS
    def printFortigateModel(self) :
        print(self._model)

    def printFortigateVersion(self) :
        print(self._version)

    def printFortigateUsername(self) :
        print(self._username)

    def printFortigatePassword(self) :
        print(self._password)

    def printFortigateConnexionCookie(self) :
        print(self._cookies)