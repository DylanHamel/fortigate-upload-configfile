#!/usr/bin/env python

import paramiko
import time
import FortiGate_Connexion
import ipAddress_MGMT
import sys
import getopt
import FortigateFirmwareFilename
import FortigateAPI


#
#
#
def main(argv) :


    # Static informations
    #pathConfigurationFile = "/Volumes/Data/fortigate_file_conf_40C_2"
    #username = "admin"
    #password = ""
    #add_ipv4_host = "192.168.1.99"


    ## Create a SSH Connextion With Fortigate
    fortigate = FortiGate_Connexion.FortiGateConnexion(username, password, add_ipv4_host)

    print("[main] update - DEBICE WILL BE UPGRADE ? ", update)
    if update :
        # We will see if we need tu upgrade Fortigate
        model, fortigateVersion = fortigate.getFortigateModelAndVersionWithAPI()

        if not version == fortigateVersion :
            # We will upgrade Fortigate in version
            versionFilename, filenameOk = FortigateFirmwareFilename.getFortigateFirmwareFilename(model, version)
            if filenameOk :
                print("[main] update - ", versionFilename, " For Fortigate ", model, " Version ", fortigateVersion , " Upgrade to ", version)
                allCommand = list()
                cmd1 = "execute restore image ftp " + str(versionFilename) + " 192.168.1.250 admin password."
                print("[main] cmd1 - ", cmd1)
                cmd2 = "y"
                allCommand.append(cmd1)
                allCommand.append(cmd2)
                time.sleep(100)
                output = fortigate.exec_cmd_for_upgrade(allCommand)
                time.sleep(2)
                print("[main] output - ", output)

                print("Please Wait 9min")
                time.sleep(60)
                print("Please Wait 8min")
                time.sleep(60)
                print("Please Wait 7min")
                time.sleep(60)
                print("Please Wait 6min")
                time.sleep(60)
                print("Please Wait 5min")
                time.sleep(60)
                print("Please Wait 4min")
                time.sleep(60)
                print("Please Wait 3min")
                time.sleep(60)
                print("Please Wait 2min")
                time.sleep(60)
                print("Please Wait 1min")
                time.sleep(60)
        else:
            print("[main]  -Your Fortigate is up to day")



    # After update, SSH Connexion will close.
    # Create a SSH Connexion
    fortigate = FortiGate_Connexion.FortiGateConnexion(username, password, add_ipv4_host)
    #output = fortigate.exec_cmd_get_output("get system status")
    #print(output)

    try :# Execute all script for push configuration
        execute(pathConfigurationFile, fortigate)


        print("[main] fortigate._interface_modified - ", fortigate._interface_modified)
        if fortigate._interface_modified :
            print("[main] - Remove Secondary IP. Wait... We will lose connection")
            print(fortigate._interface_name)
            script_remove_secondary_ip = "config system interface \n  edit " + str(fortigate._interface_name) + " \n " + \
                "set secondary-IP disable \n end \n"
            script_remove_dhcp_server = "config system dhcp server \n delete 1 \n end \n"

            execute_block(script_remove_dhcp_server, fortigate)
            execute_block(script_remove_secondary_ip, fortigate)

    except KeyboardInterrupt as e :
        print("[main] - Script has been stopped by keyboard")

    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print("[main] - Programm finished")
# --------------------------------------------------------------------------------------------------
#
# This script will remove SecondaryIP that has been set for keep connexion
#
def removeSecondaryIP(fortigate) :
    print("REMOVE")
# --------------------------------------------------------------------------------------------------
#
#
#
def execute(pathConfigurationFile, fortigate):
    """

    :param pathConfigurationFile:
    :param fortigate:
    :return:
    """
    dataFromConfigurationFile = get_file_conf(pathConfigurationFile)
    blocksConfigurationFile = get_block_conf(dataFromConfigurationFile)
    execute_blocks(blocksConfigurationFile, fortigate)
# --------------------------------------------------------------------------------------------------
#
#
#
def get_file_conf(pathConfigurationFile):
    """

    :param pathConfigurationFile:
    :return:
    """
    with open(pathConfigurationFile, 'r') as c:
        dataOfConfigurationFile = c.read()
    return dataOfConfigurationFile
# --------------------------------------------------------------------------------------------------
#
#
#
def get_block_conf (configurationFile) :
    """

    :param configurationFile:
    :return:
    """
    blocksOfConfigurationFile = configurationFile.split("\n\n")
    return blocksOfConfigurationFile
# --------------------------------------------------------------------------------------------------
#
#
#
def find_new_password (block):
    """
    This function will find new passord for user

    :param block: a block which change password for account
    :return: new password
    """
    blockExecuteSetPassword = "set password"
    password = ""
    lines = block.split("\n")

    for line in lines :
        if line.find(blockExecuteSetPassword) != -1 :
            index_d = line.find("d")
            password = line[(index_d + 2):]
            return password

# --------------------------------------------------------------------------------------------------
#
#
#
def execute_blocks(blocks, fortigate) :
    """

    :param blocks:
    :param fortigate:
    :return:
    """
    for block in blocks :
        print("Execute Block ")
        print(block)
        print("-----------------------------------------------------\n"
              "-----------------------------------------------------")

        # Check if password will change
        blockExecuteConfigSystemAdmin = "config system admin"
        blockExecuteSetPassword = "set password"
        checkWhickUserWillbeModified = "edit \""

        # Check in block if there are these 3 lines. If true, we know that password will change
        # It's possible that this alogithm dosen't work everytime
        # If you find an issue please tell me

        if (block.find(blockExecuteConfigSystemAdmin) != -1) and \
                (block.find(blockExecuteSetPassword) != -1 ) and \
                (block.find(fortigate._password) != -1):

            # Call function and change password in fortigate device for next connection
            fortigate._password = find_new_password(block)


        # These lines are used for find if your new ip address is in trusthost
        # NOT FINISHED
        filter_by_ip_source_for_admin_access = "set trusthost1"
        if block.find(filter_by_ip_source_for_admin_access) == 1 and block.find(fortigate._ipAddress) == -1 :
                print("Attention, l'adresse avec laquelle vous administrez le Fortigate n'est pas dans les \"truehost\"")
                if saisie_y_n() == False :
                    return

        # Check if ip addr will be changed
        if block.find("config system interface") != -1 and block.find("edit") != -1 :
            change_ip_addr_mgmt(block, fortigate)

        state = execute_block(block, fortigate)
        if state == False :
            return
# --------------------------------------------------------------------------------------------------
#
# Envoie une commande via SSH
#
def execute_block (command, fortigate) :
    """

    :param command:
    :param fortigate:
    :return:
    """
    chan = fortigate._sshTransport.open_session()
    print("[", fortigate._ipAddress, "]", " - ", command)
    chan.exec_command(command)
    exit_code = chan.recv_exit_status()
    stdin = chan.makefile('wb', -1)
    stdout = chan.makefile('rb', -1)
    stderr = chan.makefile_stderr('rb', -1)
    output = stdout.read()
    output = str(output, "utf-8")

    """
    # Need to be completed
    error_code_61 = "Return code -61"
    error_code_0 = "Unknown action 0"
    #
    
    if (output.find(error_code_61)) == -1 and (output.find(error_code_0)) == -1 :
        return True
    else:
        print()
        print("/!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\")
        print("Comman \"", command, " \" create an issue" )
        print("Please Check this command")
        print("/!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\")
        print(output)
        continuer = input("Would you like continue ? [y/n] : ")
        while continuer != 'y' and continuer != 'n':
            continuer = input("Please answer by [y/n] : ")

        if continuer == "y":
            return True
        else :
            return False
    """


# --------------------------------------------------------------------------------------------------
#
# SSH connexion vers le Fortigate
#
def ssh_connexion (username, password, add_ipv4_host):
    """

    :param username:
    :param password:
    :param add_ipv4_host:
    :return:
    """
    try :
        print("Try to connect on ", add_ipv4_host, " ... ")
        time.sleep(2)
        ssh = paramiko.SSHClient()
        ssh.load_system_host_keys()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(add_ipv4_host, username=username, password=password, timeout=5)
        ssh_transport = ssh.get_transport()


    except paramiko.AuthenticationException:
        print("Authentication failed, please verify your credentials")
    except paramiko.SSHException as sshException:
        print("Unable to establish SSH connection")
    except paramiko.BadHostKeyException as badHostKeyException:
        print("Unable to verify server's host key")
    except Exception as e :
        print(e.args)

    print("Connected !!!")
    return ssh_transport
# --------------------------------------------------------------------------------------------------
#
# This function ask to user informations about fortigate (username, password, ipAddress)
#
def authentification() :
    """

    :return:
    """

    new_host = input("Is it a new Fortigate with factory reset configuration ? [y/n] : ")

    while new_host != 'y' and new_host != 'n' :
        new_host = input("Please answer by [y/n] : ")

    if new_host == 'n' :
        username = input("Set username : ")
        password = input("Set password : ")
        add_ipv4_host = input("Set Fortigate IPv4 : ")
    else :
        username = "admin"
        password = ""
        add_ipv4_host = "192.168.1.99"

    path = input("Set file configuration path : ")

    return username, password, add_ipv4_host, path
# --------------------------------------------------------------------------------------------------
def saisie_y_n():
    """

    :return:
    """
    continuer = input("Would you like continue ? [y/n]")
    while continuer != 'y' and continuer != 'n':
        continuer = input("Please answer by [y/n] : ")

    if continuer == "y" :
        return True
    else :
        return False
# --------------------------------------------------------------------------------------------------
#
#
#
# def change_ip_addr_mgmt (block, ssh_transport, add_ipv4_host):
def change_ip_addr_mgmt (block, fortigate):
    """

    :param block:
    :param fortigate:
    :return:
    """
    print("========================================================================")
    print("[change_ip_addr_mgmt] Receive - |", block, "|")

    # Variables
    command_show_system_interface = "show system interface "
    command_show_system_interface_grep = " | grep ip "

    # We need to have actual ip address of interface that will be modified
    lines = block.split("\n")

    # We get interfaces that will be modified
    interface_will_be_modified = False
    for line in lines :

        # We search all lines that contains "edit"
        # If they contain "edit" we will get interface name"
        # We will create command "show system interface {intName} | grep ip"
        # If our ip address connexion is the same that we found in this command we will find on which interface
        # we are connected
        if "edit" in line :
            print("[change_ip_addr_mgmt] line - |", line, "|")
            indexEdit = line.find("edit ")
            index_first_char_of_interface = indexEdit+5

            print("[change_ip_addr_mgmt] line[index_first_char_of_interface:] - |", line[index_first_char_of_interface:], "|")

            # We execute command for show ip address of interfaces that will be modified
            command_show_sys_int = command_show_system_interface + line[index_first_char_of_interface:] + command_show_system_interface_grep

            # If we found our connection ip address in command, we kown that out ip address will be modified
            print("[change_ip_addr_mgmt] fortigate._ipAddress - |", fortigate._ipAddress, "|")
            print("[change_ip_addr_mgmt] command_show_sys_int - |", command_show_sys_int, "|")
            print("[change_ip_addr_mgmt] exec_cmd_get_output  - |", exec_cmd_get_output(command_show_sys_int, fortigate))

            findIPAddressInOutput = exec_cmd_get_output(command_show_sys_int, fortigate).find(fortigate._ipAddress)
            output = exec_cmd_get_output(command_show_sys_int, fortigate)

            print("[change_ip_addr_mgmt] findIPAddressInOutput - |", findIPAddressInOutput, "|")
            print("[change_ip_addr_mgmt] output - |", output, "|")

            if findIPAddressInOutput != -1 :
                # We know that the interface will be modified
                interface_will_be_modified = True
                # Keep interface name
                fortigate._interface_name = line[index_first_char_of_interface:]
                # Save the old ip address and netmask.
                indexSetIP = output.find("set ip ")
                indexReturnLine = output.find("\n")
                ipAdressAndNetmask = "".join(output[(indexSetIP+7):indexReturnLine])

                print("[change_ip_addr_mgmt] ipAdressAndNetmask - |", ipAdressAndNetmask, "|")

                # After this line, we have actually ip address and netmask
                # So we can assigne a new ip address (secondary interface)
                netmask = ipAddress_MGMT.extractNetmaskFromIPAddress(ipAdressAndNetmask)

                print("[change_ip_addr_mgmt] fortigate._ipAddress - |", fortigate._ipAddress, "|")
                print("[change_ip_addr_mgmt] netmask - |", netmask, "|")
                fortigate._newIPAddress = ipAddress_MGMT.generateRandomIPAddressInSubnetBoth(fortigate._ipAddress, netmask)

                print("[change_ip_addr_mgmt] fortigate._newIPAddress - |", fortigate._newIPAddress, "|")

    # Now we need to have the new ip address for this interface
    # Exclusively if interface will be modified
    need_find_set_ip = False
    edit_line = ""
    set_ip_line = ""

    print("[change_ip_addr_mgmt] interface_will_be_modified - |", interface_will_be_modified, " = ", fortigate._interface_name, "|")

    if interface_will_be_modified :
        edit_interface_will_be_modified = "edit " + str(fortigate._interface_name)
        print("[change_ip_addr_mgmt] edit_interface_will_be_modified - |", edit_interface_will_be_modified, "|")
        for line in lines :
            if edit_interface_will_be_modified in line :
                edit_line = line
                need_find_set_ip = True

            if need_find_set_ip :
                if "set ip" in line :
                    ## NEED TO DEBLOCK
                    ##
                    ##
                    ##
                    set_ip_line = line
                    print(line)
                    index_ip = line.find("ip")
                    index_first_digit_of_ip_addr_2 = index_ip + 3
                    new_add_ipv4_host = line[index_first_digit_of_ip_addr_2:]
                    need_find_set_ip = False

        print("[change_ip_addr_mgmt] fortigate._newIPAddress - |", fortigate._newIPAddress, "|")
        time.sleep(5)
        if new_add_ipv4_host.find(fortigate._ipAddress) == -1:
            # Now, if ip address will change,            # We will execute the code 2 times
            # First it will be changed interface ip address
            # Second after we are connected on the new ip it will be execute script

            # We need to create an easy script for change IP
            # and execute it
            script = "config system interface \n " + str(edit_line) + " \n " + \
                     "set secondary-IP enable \n " + "config secondaryip \n" + "edit 1 \n" + "set ip " + \
                     str(fortigate._newIPAddress) + " " +  str(netmask) +  "\n" + \
                     "set allowacces ssh https http \n " + "end \n" + \
                     "set mode static \n " + str(set_ip_line) + " \n " +  "set allowaccess ssh http https \n" + "end \n "
            print("[change_ip_addr_mgmt] script - |", script, "|")

            # Set the Secondary IP
            execute_block(script, fortigate)

            # Set a Global Variable to TRUE for that when script is finished an other script remove SecondaryIP
            fortigate._interface_modified = True

            # At this point we lose access on Fortigate.
            # ssh_transport = ssh_connexion(username, password, add_ipv4_host)
            print("[change_ip_addr_mgmt] - ", "Wait, try to reconnect with new ip : ", fortigate._newIPAddress)

            """
            # Normally this part isn't used
            # password will change by function "find_new_password" 
            continuer = input("You username or password has been changed ? [u=usernameOnly/p=passwordOnly/2=forBoth/n] : ")

            while continuer != 'p' and continuer != 'n' and continuer != '2' and continuer != 'u' :
                continuer = input("Please answer by [p/u/2/n] : ")

            if continuer == "u":
                fortigate._username = input("New username = ")
            elif continuer == "p" :
                fortigate._password = input("New password = ")
            elif continuer == "2" :
                fortigate._username = input("New username = ")
                fortigate._password = input("New password = ")
            """
            fortigate._ipAddress = fortigate._newIPAddress
            fortigate.ssh_transport()

        else :
            print("[change_ip_addr_mgmt] - ","IP Address don't change")
# --------------------------------------------------------------------------------------------------
#
# Execute command and return output
# This function will be used when you want get the output
# for a "show" command for example
#
def exec_cmd_get_output (command, fortigate) :
    """

    :param command:
    :param fortigate:
    :return:
    """
    chan = fortigate._sshTransport.open_session()
    print("[", fortigate._ipAddress , "]", " - ", command)
    chan.exec_command(command)
    exit_code = chan.recv_exit_status()
    stdin = chan.makefile('wb', -1)
    stdout = chan.makefile('rb', -1)
    stderr = chan.makefile_stderr('rb', -1)
    output = stdout.read()
    output = str(output, "utf-8")
    return  output
#
#
#
#
#
def getOptForFortigate (argv) :
    """
    -h for help
    -d for use default Forti parameters (username = admin , password = "" , ip = 192.168.1.99, sshP = 22, httpsP = 443)
    -u for username
    -p for password
    -w for configuration file path
    -v for version

    -a for define https port (default 443)
    -s for define ssh port (default 22)
    -b for download image with a usb key can only be setted if -v is setted
    -f for download image with ftp  can only be setted if -v is setted

    :param argv:
    :return:
    """

    username = ""
    password = ""
    ipaddress = ""
    pathConfigurationFile = ""
    version = ""
    sshPort = 0
    httpsPort = 0

    # Block try for block others options
    try:
        case = 0
        update = False
        opts, args = getopt.getopt(sys.argv[1:], 'dmhu:p:i:s:a:w:v:b:f:', ['--help', 'username=', 'password=', 'ip=', 'path=', '--version='])
        if sys.argv.__len__() > 2 :
            option_D_W = sys.argv[1] == "-d" and sys.argv[2] == "-w"
        else :
            option_D_W = False

        if opts.__len__() > 5 :
            raise ValueError

        elif opts.__len__() == 5 :
            if (sys.argv[1] == "-u" and sys.argv[3] == "-p" and sys.argv[5] == "-i" and sys.argv[7] == "-w" and sys.argv[1] == "-v") :
                case = 1
            else :
                raise ValueError

        elif opts.__len__() == 4 :
            if (sys.argv[1] == "-u" and sys.argv[3] == "-p" and sys.argv[5] == "-i" and sys.argv[7] == "-w") :
                case = 2
            else :
                raise ValueError

        elif opts.__len__() == 3 :
            if (sys.argv[1] == "-d" and sys.argv[2] == "-w" and sys.argv[4] == "-v") :
                case = 3
            else :
                raise ValueError

        elif opts.__len__() == 2 :
            if sys.argv[1] == "-d" and sys.argv[2] == "-w" :
                case = 4
            elif (sys.argv[1] == "-d" and sys.argv[2] == "-v") :
                case = 5
            elif (sys.argv[1] == "-m" and sys.argv[2] == "-v") :
                case = 6
            else :
                raise ValueError

        elif opts.__len__() == 1 :
            if (sys.argv[1] == "-m") :
                case = 7
            elif (sys.argv[1] == "-d") :
                case = 8
            elif (sys.argv[1] == "-h") :
                case = 9
            else :
                raise ValueError


    except getopt.GetoptError as err:
        print(str(err))
        sys.exit(2)
    except ValueError as err :
        print("Warnings parameters are missing or are not correct")
        print("use -h for help")
        sys.exit(2)


    # Set value with values
    for o, a in opts :
        if o == "-u":
            username = a
        elif o == "-p":
            password = a
        elif o == "-i":
            ipaddress = a
        elif o == "-w":
            pathConfigurationFile = a
        elif o == "-v" :
            update = True
            version = a
        elif o in ("-h" or "--help"):
            print("Please use one of these following options : ")
            print("python3.7 fortios_simple.py -h")
            print("python3.7 fortios.simple.py -u <username> -p <password> -i <ipaddress> -w <path>")
            print("python3.7 fortios.simple.py -u <username> -p <password> -i <ipaddress> -w <path> -v <version>")
            print("python3.7 fortios_simple.py -m")
            print("python3.7 fortios_simple.py -m -v <version>")
            print("python3.7 fortios_simple.py -d ")
            print("python3.7 fortios_simple.py -d -v <version>")
            print("python3.7 fortios_simple.py -d -w <path>")
            print("python3.7 fortios_simple.py -d -w <path> -v <version>")
            print("[-h] = for help, [-d] = default FortigateValues, [-m] = enter values manually")
            sys.exit(0)
        elif o == "-m":
            # Ask to user credentials
            username, password, add_ipv4_host, pathConfigurationFile = authentification()
        elif o == "-d":
            username = "admin"
            password = ""
            add_ipv4_host = "192.168.1.99"
            if not option_D_W:
                pathConfigurationFile = input("[main] - Set file configuration path : ")


    return username, password, ipaddress, pathConfigurationFile, version,  sshPort, httpsPort,

# --------------------------------------------------------------------------------------------------
#
#
#
if __name__ == "__main__":
   main(sys.argv[1:])
