import paramiko
import time
import sys
import FortigateAPI

class FortiGateConnexion :

    # Constructor With Attributes basic
    # Better to use
    def __init__(self, username, password, ipAddress):
        self._username = username
        self._password = password
        self._ipAddress = ipAddress
        self._sshTransport = self.ssh_connexion(self._username, self._password, self._ipAddress)
        self._ipAddressBck = "0.0.0.0"
        self._interface_name = ""
        self._interface_modified = False
        self._newIPAddress = "0.0.0.0"


    # Get Informations about Forigate (Model, Firmware
    def getFortigateModelAndVersion(self):
        """
        This function only works if Fortigate doesn't have char as "-", "-v", "," in hostname.
        Only use if fortigate is with factory default configuration

        :return: Fortigate model and version
        """
        print("[getFortigateModelAndVersion] Start")
        cmd_get_sys_status = "get system status | grep \"Version:\" \n"

        output = self.exec_cmd_get_output(cmd_get_sys_status)

        # Version: FortiGate-60D v.5.6.3
        index_ = output.find("-")
        index_v = output.find("v")
        index = output.find(",")

        model = output[(index_ + 1) : (index_v - 1)]
        version = output[(index_v + 1) : (index)]

        print("[getFortigateModelAndVersion] - ", model, version)
        return model, version

    def getFortigateModelAndVersionWithAPI (self):
        """
        This function use Fortigate API.

        :return: Fortigate model and version
        """

        print("[getFortigateModelAndVersionWithAPI] Start")
        print("[getFortigateModelAndVersionWithAPI] username - |", self._username, "|")
        print("[getFortigateModelAndVersionWithAPI] password - |", self._password, "|")
        print("[getFortigateModelAndVersionWithAPI] ipAddress - |", self._ipAddress, "|")

        fortigate = FortigateAPI.FortigateAPI(self._username, self._password, self._ipAddress, port=443, useHTTPS=True)

        return fortigate.getFortigateModel(), fortigate.getFortigateVersion()



    # Execute command and return output
    def exec_cmd_get_output (self, cmd):
        chan = self._sshTransport.open_session()
        print("#################################################################")
        print("[", self._ipAddress, "]", " - ", cmd)
        chan.exec_command(cmd)
        exit_code = chan.recv_exit_status()
        # print(exit_code)
        stdin = chan.makefile('wb', -1)
        stdout = chan.makefile('rb', -1)
        stderr = chan.makefile_stderr('rb', -1)
        output = stdout.read()
        output = str(output, "utf-8")
        print("#################################################################")
        return output

    def exec_cmd_for_upgrade(self, cmds):
        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(self._ipAddress, username=self._username, password=self._password, timeout=5)

        except paramiko.AuthenticationException:
            print("Authentication failed when connecting to %s" % self._ipAddress)
            sys.exit(1)
        except:
            print("Could not SSH to %s, waiting for it to start" % self._ipAddress)


        sshShell = ssh.invoke_shell()
        output = sshShell.recv(65535)
        print(output)

        for cmd in cmds :
            print("#################################################################")
            print("[", self._ipAddress, "]", " - ", cmd)
            sshShell.send(cmd + "\n")
            time.sleep(2)
            print("#################################################################")
            output = sshShell.recv(65535)
            print("[exec_cmd_for_upgrade] - ",output)

        sshShell.send("\r\r\n\n")
        output = sshShell.recv(65535)
        print("=====================================================================")
        print("[exec_cmd_for_upgrade] - ", output)
        ssh.close()


    def exec_command (self, cmd) :
        return


    # Setup SSH Connexion
    def ssh_transport(self):
        self._sshTransport = self.ssh_connexion(self._username, self._password, self._ipAddress)

    # Disconnect SSH Connexion
    def ssh_disconnect(self):
        self._sshTransport.close()


    # Create a SSH Connexion with host
    def ssh_connexion(self, username, password, add_ipv4_host):
        try:
            print("Try to connect on", add_ipv4_host)
            time.sleep(2)
            ssh = paramiko.SSHClient()
            ssh.load_system_host_keys()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(add_ipv4_host, username=username, password=password, timeout=5)
            ssh_transport = ssh.get_transport()


        except paramiko.AuthenticationException:
            print("Authentication failed, please verify your credentials")
        except paramiko.SSHException as sshException:
            print("Unable to establish SSH connection")
        except paramiko.BadHostKeyException as badHostKeyException:
            print("Unable to verify server's host key")
        except Exception as e:
            print(e.args)
        else :
            print("Connected on IPv4 ", self._ipAddress)

        return ssh_transport


    # GETTER
    def get_username(self):
        return self._username
    #
    def get_password(self):
        return self._password
    #
    def get_ipAddress(self):
        return self._get_ipAddress
    #
    def get_sshTransport(self):
        return self._sshTransport
    #
    def get_ipAddressBcK(self):
        return self._ipAddressBck

    # SETTER
    def get_username(self, username):
        self._username = username
    #
    def get_password(self, password):
        self._password = password
    #
    def get_ipAddress(self, ipAddress):
        self._ipAddress = ipAddress
    #
    def get_sshTransport(self, sshTransport):
        self._sshTransport = sshTransport
    #
    def get_ipAddressBcK(self, ipAddressBck):
        self._ipAddressBck = ipAddressBck

