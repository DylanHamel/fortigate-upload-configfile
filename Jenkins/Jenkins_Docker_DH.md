

# Jenkins - Docker

###### Dylan Hamel - 11 / 09 / 2018 - contact@dylan-hamel.ch




## 1. Start Jenkins in a Docker Container

```bash
docker search jenkins
docker pull jenkins
```

Run container :

```bash
docker run -d -p 8080:8080 --name jenkins jenkins
```



Connect with web navigator http://localhost:8080.

![start01.png](./images/start01.png)
For have password 

```bash
docker exec -it jenkins /bin/bash

jenkins@24caf83f7ef2:/$ cat /var/jenkins_home/secrets/initialAdminPassword 
e9db6d84289242f2a93aeba46f322a20 # Copy-Paste this password on start page

```



## Run a Python script with Jenkins

Your script need to begin by 

```bash
#!/usr/bin/env python
```

Script permissions need to be executable

```bash
-rwxr-xr-x  1 dylan.hamel  admin    14K 10 sep 14:22 fortios_simple.py
```

Test your script in your terminal 

```bash
➜  FortigatePushOutputConf git:(master) ✗ ./fortios_simple.py 
Is it a new Fortigate with factory reset configuration ? [y/n] : 
```

On ansible.dh.local

```bash
yum -y install python34
yum -y install python-pip
pip install --upgrade pip
pip install paramiko
# if it doesn't work use
yum -y install python34-paramiko.noarch
# Test with 
# [root@ansible FortigatePushOutputConf]# python3
# Python 3.4.9 (default, Aug 14 2018, 21:28:57) 
# [GCC 4.8.5 20150623 (Red Hat 4.8.5-28)] on linux
# Type "help", "copyright", "credits" or "license" for more information.
# >>> import paramiko
# >>> exit()
```

```bash
[root@ansible FortigatePushOutputConf]# python3 fortios_simple.py -d -w /root
Try to connect on 192.168.1.99  ... 
```



<u>This script is on host (ansible.dh.local / 172.16.194.150/24)</u>

We need to run a command on this host



On Jenkins go to "Manage Jenkins"
Add plugin ```Publish Over SSH```

![install_ssh.png](./images/install_ssh.png)

You can too install when you start Jenkins

![start02.png](./images/start02.png)

![install_ssh.png](./images/install_ssh.png)

You can too install when you start Jenkins

![start02.png](./images/start02.png)

Then you need to add informations for manage Host



1. Create SSH Key

```bash
jenkins@24caf83f7ef2:/$ ssh-keygen -t rsa -b 4096 
Generating public/private rsa key pair.
Enter file in which to save the key (/var/jenkins_home/.ssh/id_rsa): 
Created directory '/var/jenkins_home/.ssh'.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /var/jenkins_home/.ssh/id_rsa.
Your public key has been saved in /var/jenkins_home/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:TkU69a4CXokz2NqRDV7T3H6X/jBh1SusiEtVexCEQ8k jenkins@24caf83f7ef2
The key's randomart image is:
+---[RSA 4096]----+
|        o.*o     |
|         E o.   .|
|      . = =oo   o|
|     + * =.o+  .o|
|    . X S. .o++o.|
|     + Oo ..oooo |
|    . ooo...  o. |
|      . ..     o.|
|       .        .|
+----[SHA256]-----+
```



2. Copy publique Key on ansible.dh.local

```bash
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/var/jenkins_home/.ssh/id_rsa.pub"
The authenticity of host '172.16.194.150 (172.16.194.150)' can't be established.
ECDSA key fingerprint is SHA256:6RMxDITKvc7Int6NGAkuFMmgUx5/Ydt+3NQuRhuxkts.
Are you sure you want to continue connecting (yes/no)? yes
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
root@172.16.194.150's password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'root@172.16.194.150'"
and check to make sure that only the key(s) you wanted were added.

jenkins@24caf83f7ef2:/$ 
```



3. Add an host on Jenkins.

   Manage Jenkins --> Configure Sytem

![jenkins_host_ssh.png](./images/jenkins_host_ssh.png)



#### Now we can create a Jenkins Task

New Item --> Build a free-style software project (name it) --> Click "Ok"



Reminder : we would like run script with command :

```bash
python3 fortios_simple.py -d -w <fileConfigurationPath>
```

We therefore need a parameter

![build_01.png](./images/build_01.png)

Then give command for run script on ansible.dh.local

![build02.png](./images/build02.png)

=> Save



On ansible.dh.local we had clone repo.
Run a build 
![build03.png](./images/build03.png)

Click "Build"

![build04.png](./images/build04.png)

![output_01.png](./images/output_01.png)

We see that the path are correct but no device with ip address 192.168.1.99 was not found



## Execute Script in a Python Container

Prérequis :

* Avoir installer VirtualBox

```bash
docker-machine create --driver=virtualbox Docker
docker-machine ssh Docker

# Have informations about docker-machine
docker-machine inspect Docker
```



On Jenkins 

```bash
ssh-copy-id -i ~/.ssh/id_rsa.pub docker@192.168.99.100
# pass=tcuser
```



Add host 

![jenkins01.png](./images/jenkins01.png)

Pull docker.io/python

```bash
docker search python
docker pull python

Using default tag: latest
latest: Pulling from library/python
05d1a5232b46: Pull complete 
5cee356eda6b: Pull complete 
89d3385f0fd3: Pull complete 
80ae6b477848: Pull complete 
28bdf9e584cc: Pull complete 
523b203f62bd: Pull complete 
e423ae9d5ac7: Pull complete 
adc78e8180f7: Pull complete 
5c4f0bc7295a: Pull complete 
Digest: sha256:68dc1ce187dd2c32f4b237e44610d9f4f34add97f9c5c7c92268db14c77fb5c2
Status: Downloaded newer image for python:latest
```



Create environnement

```bash
mkdir Fortigate
cd Fortigate
pwd
# /home/docker/Fortigate
```



Recupere Script on Github

```bash
git clone https://github.com/DylanHamel/FortigatePushOutputConf.git
Cloning into 'FortigatePushOutputConf'...
```



Prepare new Python image

```bash
touch Dockerfile
vi Dockerfile
```



```dockerfile
FROM python:3

RUN pip install paramiko
```



Create a new image

```bash
docker@Docker:~/Fortigate$ docker build -t dh/python-paramiko .
Sending build context to Docker daemon  363.5kB
Step 1/2 : FROM python:3
 ---> a9d071760c82
Step 2/2 : RUN pip install paramiko
 ---> Running in f25c407c6cac
Collecting paramiko
  Downloading https://files.pythonhosted.org/packages/3e/db/cb7b6656e0e7387637ce850689084dc0b94b44df31cc52e5fc5c2c4fd2c1/paramiko-2.4.1-py2.py3-none-any.whl (194kB)
Collecting pynacl>=1.0.1 (from paramiko)
  Downloading https://files.pythonhosted.org/packages/c0/58/e514997c900f75a6b0c1918794e115092504aff58b26e14476727e65fdf6/PyNaCl-1.2.1-cp37-cp37m-manylinux1_x86_64.whl (689kB)
Collecting cryptography>=1.5 (from paramiko)
  Downloading https://files.pythonhosted.org/packages/59/32/92cade62c645756a83598edf56289e9b19aae5370642a7ce690cd06bc72f/cryptography-2.3.1-cp34-abi3-manylinux1_x86_64.whl (2.1MB)
Collecting pyasn1>=0.1.7 (from paramiko)
  Downloading https://files.pythonhosted.org/packages/d1/a1/7790cc85db38daa874f6a2e6308131b9953feb1367f2ae2d1123bb93a9f5/pyasn1-0.4.4-py2.py3-none-any.whl (72kB)
Collecting bcrypt>=3.1.3 (from paramiko)
  Downloading https://files.pythonhosted.org/packages/a8/ce/1b9fe8001f95191a6254736d502fa51a2ff6bd0ffa9e290640f909b3adcb/bcrypt-3.1.4-cp34-abi3-manylinux1_x86_64.whl (51kB)
Collecting six (from pynacl>=1.0.1->paramiko)
  Downloading https://files.pythonhosted.org/packages/67/4b/141a581104b1f6397bfa78ac9d43d8ad29a7ca43ea90a2d863fe3056e86a/six-1.11.0-py2.py3-none-any.whl
Collecting cffi>=1.4.1 (from pynacl>=1.0.1->paramiko)
  Downloading https://files.pythonhosted.org/packages/51/7b/d1014289d0578c3522b2798b9cb87c65e5b36798bd3ae68a75fa1fe09e78/cffi-1.11.5-cp37-cp37m-manylinux1_x86_64.whl (421kB)
Collecting asn1crypto>=0.21.0 (from cryptography>=1.5->paramiko)
  Downloading https://files.pythonhosted.org/packages/ea/cd/35485615f45f30a510576f1a56d1e0a7ad7bd8ab5ed7cdc600ef7cd06222/asn1crypto-0.24.0-py2.py3-none-any.whl (101kB)
Collecting idna>=2.1 (from cryptography>=1.5->paramiko)
  Downloading https://files.pythonhosted.org/packages/4b/2a/0276479a4b3caeb8a8c1af2f8e4355746a97fab05a372e4a2c6a6b876165/idna-2.7-py2.py3-none-any.whl (58kB)
Collecting pycparser (from cffi>=1.4.1->pynacl>=1.0.1->paramiko)
  Downloading https://files.pythonhosted.org/packages/8c/2d/aad7f16146f4197a11f8e91fb81df177adcc2073d36a17b1491fd09df6ed/pycparser-2.18.tar.gz (245kB)
Building wheels for collected packages: pycparser
  Running setup.py bdist_wheel for pycparser: started
  Running setup.py bdist_wheel for pycparser: finished with status 'done'
  Stored in directory: /root/.cache/pip/wheels/c0/a1/27/5ba234bd77ea5a290cbf6d675259ec52293193467a12ef1f46
Successfully built pycparser
Installing collected packages: six, pycparser, cffi, pynacl, asn1crypto, idna, cryptography, pyasn1, bcrypt, paramiko
Successfully installed asn1crypto-0.24.0 bcrypt-3.1.4 cffi-1.11.5 cryptography-2.3.1 idna-2.7 paramiko-2.4.1 pyasn1-0.4.4 pycparser-2.18 pynacl-1.2.1 six-1.11.0
Removing intermediate container f25c407c6cac
 ---> cda729c11528
Successfully built cda729c11528
Successfully tagged dh/python-paramiko:latest
docker@Docker:~/Fortigate$ 
```



Run container 

```bash
docker@Docker:~/Fortigate$ docker run -it --rm --name run-fortios -v /home/docker/Fortigate/FortigatePushOutputConf:/usr/src/widget_app dh/python-paramiko:latest python /usr/src/widget_app/fortios_simple.py -d -w /path
Try to connect on 192.168.1.99  ... 
```



Task Jenkins

![docker_jenkins.png](./images/docker_jenkins.png)